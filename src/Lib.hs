module Lib
    ( someFunc
    ) where
    
import qualified Data.Text(Text, unpack)

overloadedString:: Data.Text.Text -> Data.Text.Text
overloadedString = (<>) "hello " 

someFunc :: IO ()
someFunc = putStrLn $ Data.Text.unpack $ overloadedString "world"
